import {Component} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public opcionesMenu = [    
    {name: 'COMERCIOS', url: '/comercios', icon: 'restore'},
    {name: 'SERVICIOS', url: '/servicios', icon: 'restore'},
    {name: 'TURNOS', url: '/turnos', icon: 'restore'},
  ];

  constructor(private router: Router) {
  }

  abrir(url: string): void {
    this.router.navigate([url]);
  }
}
