import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ServiciosTableComponent} from './servicios-table/servicios-table.component';
import {MaterialModule} from "../../material/material.module";
import {MatTableModule} from "@angular/material/table";
import {UtilsModule} from "../../utils/utils.module";
import {CreateServiciosModalComponent} from './create-servicios-modal/create-servicios-modal.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    ServiciosTableComponent,
    CreateServiciosModalComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    MatTableModule,
    UtilsModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class ServiciosModule {
}
