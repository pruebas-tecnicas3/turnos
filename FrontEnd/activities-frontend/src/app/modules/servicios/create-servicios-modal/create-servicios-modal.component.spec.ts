import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateServiciosModalComponent } from './create-servicios-modal.component';

describe('CreateComerciosModalComponent', () => {
  let component: CreateServiciosModalComponent;
  let fixture: ComponentFixture<CreateServiciosModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateServiciosModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateServiciosModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
