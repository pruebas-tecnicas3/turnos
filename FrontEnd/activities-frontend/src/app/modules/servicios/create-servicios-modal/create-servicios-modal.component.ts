import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import { Comercios, Servicios } from 'src/app/core/entities/entidades';
import { ServiciosService } from 'src/app/core/services/servicios.service';
import { ComerciosService } from 'src/app/core/services/comercio.service';

@Component({
  selector: 'app-create-servicios-modal',
  templateUrl: './create-servicios-modal.component.html',
  styleUrls: ['./create-servicios-modal.component.css']
})
export class CreateServiciosModalComponent {
  comerciosForm: FormGroup;
  submited: boolean = false;  
  currentServicios: Servicios;
  idSubject: number = 0;
  comercios: Comercios[] = [];
  comerciosCreate : Comercios = new Comercios;

  constructor(public dialogRef: MatDialogRef<CreateServiciosModalComponent>,
              private formBuilder: FormBuilder,
              private serviciosService: ServiciosService,
              private snackBar: MatSnackBar,
              private comercioService: ComerciosService,
              @Inject(MAT_DIALOG_DATA) public servicios: Servicios ) {
                this.currentServicios = servicios;    
    this.comerciosForm = this.formBuilder.group({      
      duracion: ['', Validators.required],
      hora_apertura: ['', Validators.required],
      hora_cierre: ['', Validators.required],
      nom_servicio: ['', Validators.required],
      comercios: ['', Validators.required],
    });
  }

  ngOnInit() {
    this.comercioService.getAllComercios().subscribe(comercios => {
      this.comercios = comercios;
    });
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  aceptar() {
    debugger     
    this.submited = true;
    this.comerciosForm.markAllAsTouched();
    console.log('Formulario'+this.comerciosForm);
    console.log('Formulario'+this.comerciosForm.invalid);
    debugger
    if (this.comerciosForm.invalid) {
      return;
    }
    const servicios: Servicios = new Servicios();      
    servicios.duracion = this.f['duracion'].value;
    servicios.hora_apertura = this.f['hora_apertura'].value;
    servicios.hora_cierre = this.f['hora_cierre'].value;
    servicios.nom_servicio = this.f['nom_servicio'].value;
    this.comerciosCreate.id_comercio = this.f['comercios'].value;
    servicios.comercio = this.comerciosCreate;
    debugger
    this.serviciosService.registerServicios(servicios).subscribe(response => {
      debugger
      this.openSnackBar(response.message, 3000, 'success');
      this.dialogRef.close(response.objectResponse as Servicios);
    }, error => {
      this.openSnackBar(error.error.message, 3000, 'warning');
    });
  }

  private openSnackBar(mensaje: string, duracion: number, color: string): void {
    this.snackBar.open(mensaje, 'Ok', {
      duration: duracion,
      horizontalPosition: `center`,
      verticalPosition: `top`,
      panelClass: [color]
    });
  }

  validateRequired(control: AbstractControl) {
    return control.hasError('required') && (control.touched || this.submited);
  }

  get f() {
    return this.comerciosForm.controls;
  }

  selectSubject(event: any) {
    this.idSubject = event;
  }

}
