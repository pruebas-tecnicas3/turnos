import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import { Servicios } from 'src/app/core/entities/entidades';
import { ServiciosService } from 'src/app/core/services/servicios.service';
import { CreateServiciosModalComponent } from '../create-servicios-modal/create-servicios-modal.component';

@Component({
  selector: 'app-servicios-table',
  templateUrl: './servicios-table.component.html',
  styleUrls: ['./servicios-table.component.css']
})
export class ServiciosTableComponent implements OnInit {
  displayedColumns: string[] = ['id_servicio', 'duracion', 'hora_apertura', 'hora_cierre', 'nom_servicio', 'comercio', 'accion'];
  dataSource!: MatTableDataSource<Servicios>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  servicios: Servicios[] = [];

  constructor(private serviciosService: ServiciosService,
              private matDialog: MatDialog,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.serviciosService.getAllServicios().subscribe(servicios => {
      this.servicios = servicios;
      this.dataSource = new MatTableDataSource<Servicios>(this.servicios);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addServicios() {
    const dialogRef = this.matDialog.open(CreateServiciosModalComponent, {
      width: '31rem',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(servicios => {
      if (servicios) {
        this.servicios.push(servicios);
        this.dataSource = new MatTableDataSource<Servicios>(this.servicios);
        this.dataSource.paginator = this.paginator;
        this.ngOnInit();
      }
    });
  }
  
  deleteServicios(servicios: Servicios) {
    this.serviciosService.deleteServicios(servicios).subscribe(response => {
      this.openSnackBar(response.message, 3000, 'success');
      this.ngOnInit();
    }, error => {
      this.openSnackBar(error.error.message, 3000, 'warning');
    });
  }

  private openSnackBar(mensaje: string, duracion: number, color: string): void {
    this.snackBar.open(mensaje, 'Ok', {
      duration: duracion,
      horizontalPosition: `center`,
      verticalPosition: `top`,
      panelClass: [color]
    });
  }
  
}
