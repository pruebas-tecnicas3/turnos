import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Turnos } from 'src/app/core/entities/entidades';
import { CreateTurnosModalComponent } from '../create-turnos-modal/create-turnos-modal.component';
import { TurnosService } from 'src/app/core/services/turnos.service';

@Component({
  selector: 'app-turnos-table',
  templateUrl: './turnos-table.component.html',
  styleUrls: ['./turnos-table.component.css']
})
export class TurnosTableComponent implements OnInit {
  displayedColumns: string[] = ['id_turno', 'id_servicio', 'fecha_turno', 'hora_inicio', 'hora_fin', 'estado', 'accion'];
  dataSource!: MatTableDataSource<Turnos>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  turnos: Turnos[] = [];

  constructor(private turnosService: TurnosService,
              private matDialog: MatDialog,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.turnosService.getAllTurnos().subscribe(turnos => {
      this.turnos = turnos;
      this.dataSource = new MatTableDataSource<Turnos>(this.turnos);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addTurnos() {
    const dialogRef = this.matDialog.open(CreateTurnosModalComponent, {
      width: '31rem',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(turnos => {
      if (turnos) {
        this.turnos.push(turnos);
        this.dataSource = new MatTableDataSource<Turnos>(this.turnos);
        this.dataSource.paginator = this.paginator;
        this.ngOnInit();
      }
    });
  }
  
  deleteTurnos(turnos: Turnos) {
    this.turnosService.deleteTurnos(turnos).subscribe(response => {
      this.openSnackBar(response.message, 3000, 'success');
      this.ngOnInit();
    }, error => {
      this.openSnackBar(error.error.message, 3000, 'warning');
    });
  }

  private openSnackBar(mensaje: string, duracion: number, color: string): void {
    this.snackBar.open(mensaje, 'Ok', {
      duration: duracion,
      horizontalPosition: `center`,
      verticalPosition: `top`,
      panelClass: [color]
    });
  }
  
}
