import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TurnosTableComponent } from './turnos-table.component';

describe('ComerciosTableComponent', () => {
  let component: TurnosTableComponent;
  let fixture: ComponentFixture<TurnosTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TurnosTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TurnosTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
