import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTurnosModalComponent } from './create-turnos-modal.component';

describe('CreateTurnosModalComponent', () => {
  let component: CreateTurnosModalComponent;
  let fixture: ComponentFixture<CreateTurnosModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateTurnosModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateTurnosModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
