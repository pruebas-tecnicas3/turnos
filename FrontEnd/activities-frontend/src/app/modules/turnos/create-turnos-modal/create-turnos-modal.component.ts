import {Component, Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import { Comercios, Servicios, Turnos } from 'src/app/core/entities/entidades';
import { TurnosService } from 'src/app/core/services/turnos.service';
import { ServiciosService } from 'src/app/core/services/servicios.service';
import { ComerciosService } from 'src/app/core/services/comercio.service';

@Component({
  selector: 'app-create-turnos-modal',
  templateUrl: './create-turnos-modal.component.html',
  styleUrls: ['./create-turnos-modal.component.css']
})
export class CreateTurnosModalComponent {
  serviciosForm: FormGroup;
  submited: boolean = false;  
  currentTurnos: Turnos;
  idSubject: number = 0;
  servicio: Servicios[] = [];
  comercio: Comercios[] = [];
  servicioCreate : Servicios = new Servicios;

  constructor(public dialogRef: MatDialogRef<CreateTurnosModalComponent>,
              private formBuilder: FormBuilder,
              private turnosService: TurnosService,
              private snackBar: MatSnackBar,
              private servicioService: ServiciosService,
              private comercioService: ComerciosService,
              @Inject(MAT_DIALOG_DATA) public turnos: Turnos ) {
                this.currentTurnos = turnos;    
    this.serviciosForm = this.formBuilder.group({            
      estado: ['', Validators.required],
      fecha_turno: ['', Validators.required],      
      hora_fin: ['', Validators.required],
      hora_inicio: ['', Validators.required],      
      comercio: ['', Validators.required],
      servicio: ['', Validators.required],
    });
  }

  ngOnInit() {    
    this.comercioService.getAllComercios().subscribe(comercio => {
      this.comercio = comercio;
    });
    this.servicioService.getAllServicios().subscribe(servicios => {
      this.servicio = servicios;
    });
  }

  onSelectChange(event: any): void {
    debugger
    this.servicioService.getServiciosPorIdComercio(2).subscribe(servicios => {
      this.servicio = servicios;
    });
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  aceptar() {
    debugger     
    this.submited = true;
    this.serviciosForm.markAllAsTouched();
    debugger
    /*if (this.serviciosForm.invalid) {
      return;
    }*/
    const turnos: Turnos = new Turnos();          
    turnos.fecha_turno = this.f['fecha_turno'].value;
    turnos.hora_inicio = this.f['hora_inicio'].value;
    turnos.hora_fin = this.f['hora_fin'].value;
    turnos.estado = 'A';    
    this.servicioCreate.id_servicio = this.f['servicio'].value;
    turnos.servicio = this.servicioCreate;

    debugger
    this.turnosService.registerTurnos(turnos).subscribe(response => {
      debugger
      this.openSnackBar(response.message, 3000, 'success');
      this.dialogRef.close(response.objectResponse as Turnos);
    }, error => {
      this.openSnackBar(error.error.message, 3000, 'warning');
    });
  }

  private openSnackBar(mensaje: string, duracion: number, color: string): void {
    this.snackBar.open(mensaje, 'Ok', {
      duration: duracion,
      horizontalPosition: `center`,
      verticalPosition: `top`,
      panelClass: [color]
    });
  }

  validateRequired(control: AbstractControl) {
    return control.hasError('required') && (control.touched || this.submited);
  }

  get f() {
    return this.serviciosForm.controls;
  }

  selectSubject(event: any) {
    this.idSubject = event;
  }

}
