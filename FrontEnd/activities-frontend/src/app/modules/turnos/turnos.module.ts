import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MaterialModule} from "../../material/material.module";
import {MatTableModule} from "@angular/material/table";
import {UtilsModule} from "../../utils/utils.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { TurnosTableComponent } from './turnos-table/turnos-table.component';
import { CreateTurnosModalComponent } from './create-turnos-modal/create-turnos-modal.component';

@NgModule({
  declarations: [
    TurnosTableComponent,
    CreateTurnosModalComponent
  ],
  imports: [
    CommonModule,
    MaterialModule,
    MatTableModule,
    UtilsModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class TurnosModule {
}
