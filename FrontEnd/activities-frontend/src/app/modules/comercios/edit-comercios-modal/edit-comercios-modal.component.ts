import {Component,Inject} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from "@angular/material/dialog";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import { Comercios } from 'src/app/core/entities/entidades';
import { ComerciosService } from 'src/app/core/services/comercio.service';

@Component({
  selector: 'app-edit-comercios-modal',
  templateUrl: './edit-comercios-modal.component.html',
  styleUrls: ['./edit-comercios-modal.component.css']
})
export class EditComerciosModalComponent {
  comerciosForm: FormGroup;
  submited: boolean = false;
  currentComercio: Comercios;

  constructor(public dialogRef: MatDialogRef<EditComerciosModalComponent>,
              private formBuilder: FormBuilder,
              private comerciosService: ComerciosService,
              private snackBar: MatSnackBar,
              @Inject(MAT_DIALOG_DATA) public comercios: Comercios) {
      this.currentComercio = comercios;
      this.comerciosForm = this.formBuilder.group({
      id_comercio: ['', Validators.required],
      aforo_maximo: ['', Validators.required],
      nom_comercio: ['', Validators.required]      
    });
  }

  ngOnInit() {
    this.comerciosForm.patchValue({
      id_comercio: this.currentComercio.id_comercio,
      aforo_maximo: this.currentComercio.aforo_maximo,
      nom_comercio: this.currentComercio.nom_comercio      
    })
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  aceptar() {
    this.submited = true;
    this.comerciosForm.markAllAsTouched();
    if (this.comerciosForm.invalid) {
      return;
    }

    const comercio: Comercios = new Comercios();
    comercio.id_comercio = this.f['id_comercio'].value;
    comercio.aforo_maximo = this.f['aforo_maximo'].value;
    comercio.nom_comercio = this.f['nom_comercio'].value;    
    this.comerciosService.updateComercio(comercio).subscribe(response => {
      this.openSnackBar(response.message, 3000, 'success');
      this.dialogRef.close(response.objectResponse as Comercios);
    }, error => {
      this.openSnackBar(error.error.message, 3000, 'warning');
    });
  }

  private openSnackBar(mensaje: string, duracion: number, color: string): void {
    this.snackBar.open(mensaje, 'Ok', {
      duration: duracion,
      horizontalPosition: `center`,
      verticalPosition: `top`,
      panelClass: [color]
    });
  }

  validateRequired(control: AbstractControl) {
    return control.hasError('required') && (control.touched || this.submited);
  }

  get f() {
    return this.comerciosForm.controls;
  }
}
