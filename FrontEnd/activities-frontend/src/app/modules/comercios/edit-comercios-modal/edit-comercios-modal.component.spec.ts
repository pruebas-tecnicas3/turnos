import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditComerciosModalComponent } from './edit-comercios-modal.component';

describe('CreateComerciosModalComponent', () => {
  let component: EditComerciosModalComponent;
  let fixture: ComponentFixture<EditComerciosModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditComerciosModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditComerciosModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
