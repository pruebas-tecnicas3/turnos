import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import { Comercios } from 'src/app/core/entities/entidades';

@Component({
  selector: 'app-details-modal',
  templateUrl: './details-modal.component.html',
  styleUrls: ['./details-modal.component.css']
})
export class DetailsModalComponent {

  currentComercio: Comercios;

  constructor(public dialogRef: MatDialogRef<DetailsModalComponent>,
              @Inject(MAT_DIALOG_DATA) public comercio: Comercios) {
    this.currentComercio = comercio;
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  viewSubjectRegister(): void {
    this.dialogRef.close(this.currentComercio.id_comercio);
  }
}
