import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateComerciosModalComponent } from './create-comercios-modal.component';

describe('CreateComerciosModalComponent', () => {
  let component: CreateComerciosModalComponent;
  let fixture: ComponentFixture<CreateComerciosModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateComerciosModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CreateComerciosModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
