import {Component} from '@angular/core';
import {MatDialogRef} from "@angular/material/dialog";
import {AbstractControl, FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import { ComerciosService } from 'src/app/core/services/comercio.service';
import { Comercios } from 'src/app/core/entities/entidades';

@Component({
  selector: 'app-create-comercios-modal',
  templateUrl: './create-comercios-modal.component.html',
  styleUrls: ['./create-comercios-modal.component.css']
})
export class CreateComerciosModalComponent {
  comerciosForm: FormGroup;
  submited: boolean = false;

  constructor(public dialogRef: MatDialogRef<CreateComerciosModalComponent>,
              private formBuilder: FormBuilder,
              private comerciosService: ComerciosService,
              private snackBar: MatSnackBar) {
    this.comerciosForm = this.formBuilder.group({
      nom_comercio: ['', Validators.required],
      aforo_maximo: ['', Validators.required]
    });
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  aceptar() {
    this.submited = true;
    this.comerciosForm.markAllAsTouched();
    if (this.comerciosForm.invalid) {
      return;
    }

    const comercio: Comercios = new Comercios();
    comercio.aforo_maximo = this.f['aforo_maximo'].value;
    comercio.nom_comercio = this.f['nom_comercio'].value;
    this.comerciosService.registerComercios(comercio).subscribe(response => {
      this.openSnackBar(response.message, 3000, 'success');
      this.dialogRef.close(response.objectResponse as Comercios);
    }, error => {
      this.openSnackBar(error.error.message, 3000, 'warning');
    });
  }

  private openSnackBar(mensaje: string, duracion: number, color: string): void {
    this.snackBar.open(mensaje, 'Ok', {
      duration: duracion,
      horizontalPosition: `center`,
      verticalPosition: `top`,
      panelClass: [color]
    });
  }

  validateRequired(control: AbstractControl) {
    return control.hasError('required') && (control.touched || this.submited);
  }

  get f() {
    return this.comerciosForm.controls;
  }
}
