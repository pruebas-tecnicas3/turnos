import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ComerciosTableComponent} from './comercios-table/comercios-table.component';
import {MaterialModule} from "../../material/material.module";
import {MatTableModule} from "@angular/material/table";
import {UtilsModule} from "../../utils/utils.module";
import {CreateComerciosModalComponent} from './create-comercios-modal/create-comercios-modal.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {DetailsModalComponent} from './details-comercios-modal/details-modal.component';
import {EditComerciosModalComponent} from './edit-comercios-modal/edit-comercios-modal.component';

@NgModule({
  declarations: [
    ComerciosTableComponent,
    CreateComerciosModalComponent,
    DetailsModalComponent,
    EditComerciosModalComponent,
  ],
  imports: [
    CommonModule,
    MaterialModule,
    MatTableModule,
    UtilsModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class ComerciosModule {
}
