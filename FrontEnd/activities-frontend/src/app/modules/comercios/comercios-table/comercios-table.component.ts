import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatDialog} from "@angular/material/dialog";
import {CreateComerciosModalComponent} from "../create-comercios-modal/create-comercios-modal.component";
import {DetailsModalComponent} from "../details-comercios-modal/details-modal.component";
import {EditComerciosModalComponent} from "../edit-comercios-modal/edit-comercios-modal.component";
import {MatSnackBar} from "@angular/material/snack-bar";
import { ComerciosService } from 'src/app/core/services/comercio.service';
import { Comercios } from 'src/app/core/entities/entidades';

@Component({
  selector: 'app-comercios-table',
  templateUrl: './comercios-table.component.html',
  styleUrls: ['./comercios-table.component.css']
})
export class ComerciosTableComponent implements OnInit {
  displayedColumns: string[] = ['id_comercio', 'aforo_maximo', 'nom_comercio', 'accion'];
  dataSource!: MatTableDataSource<Comercios>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  comercios: Comercios[] = [];

  constructor(private comercioService: ComerciosService,
              private matDialog: MatDialog,
              private snackBar: MatSnackBar) {
  }

  ngOnInit() {
    this.comercioService.getAllComercios().subscribe(comercios => {
      this.comercios = comercios;
      this.dataSource = new MatTableDataSource<Comercios>(this.comercios);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  addComercios() {
    const dialogRef = this.matDialog.open(CreateComerciosModalComponent, {
      width: '31rem',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(comercios => {
      if (comercios) {
        this.comercios.push(comercios);
        this.dataSource = new MatTableDataSource<Comercios>(this.comercios);
        this.dataSource.paginator = this.paginator;
      }
    });
  }

  showDetails(comercios: Comercios) {
    console.log(comercios);
    const dialogRef = this.matDialog.open(DetailsModalComponent, {
      width: '31rem',
      data: comercios,
      disableClose: true
    });
  }

  deleteComercios(comercio: Comercios) {
    this.comercioService.deleteComercios(comercio).subscribe(response => {
      this.openSnackBar(response.message, 3000, 'success');
      this.ngOnInit();
    }, error => {
      this.openSnackBar(error.error.message, 3000, 'warning');
    });
  }

  private openSnackBar(mensaje: string, duracion: number, color: string): void {
    this.snackBar.open(mensaje, 'Ok', {
      duration: duracion,
      horizontalPosition: `center`,
      verticalPosition: `top`,
      panelClass: [color]
    });
  }

  editarComercios(comercios: Comercios){
    const dialogRef = this.matDialog.open(EditComerciosModalComponent, {
      width: '31rem',
      data: comercios,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(comercios => {
      if (comercios) {
        this.ngOnInit();
      }
    });
  }
}
