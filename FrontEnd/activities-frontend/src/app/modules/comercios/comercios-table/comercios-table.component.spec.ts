import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComerciosTableComponent } from './comercios-table.component';

describe('ComerciosTableComponent', () => {
  let component: ComerciosTableComponent;
  let fixture: ComponentFixture<ComerciosTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComerciosTableComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ComerciosTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
