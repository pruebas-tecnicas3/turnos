import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ComerciosTableComponent} from './modules/comercios/comercios-table/comercios-table.component';
import { ServiciosTableComponent } from './modules/servicios/servicios-table/servicios-table.component';
import { TurnosTableComponent } from './modules/turnos/turnos-table/turnos-table.component';

const routes: Routes = [  
  {path: 'comercios', component:ComerciosTableComponent},
  {path: 'servicios', component:ServiciosTableComponent},
  {path: 'turnos', component:TurnosTableComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
