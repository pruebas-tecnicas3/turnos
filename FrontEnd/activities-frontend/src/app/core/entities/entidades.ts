export class Comercios {
  id_comercio!: number;
  aforo_maximo!: number;
  nom_comercio!: string;  

  constructor() {
  }
}

export class Servicios {
  id_servicio!: number;
  duracion!: number;
  hora_apertura!: string;  
  hora_cierre!: string;  
  nom_servicio!: string;  
  comercio!: Comercios;  

  constructor() {
  }
}

export class Turnos {
  id_turno!: number;
  id_servicio!: number;
  fecha_turno!: string;  
  hora_inicio!: string;  
  hora_fin!: string;  
  estado!: string;    
  servicio!: Servicios;  

  constructor() {
  }
}

