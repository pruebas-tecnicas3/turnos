import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Response} from "../entities/response";
import { Comercios } from '../entities/entidades';

export const ipServer = 'http://localhost:8085/api';

@Injectable({
  providedIn: 'root'
})
export class ComerciosService {

  constructor(private http: HttpClient) {
  }

  public getAllComercios(): Observable<Comercios[]> {
    return this.http.get<Comercios[]>(`${ipServer}/listComercios`);
  }

  public registerComercios(comercios: Comercios): Observable<Response> {
    return this.http.post<Response>(`${ipServer}/registrarComercios`, comercios);
  }

  public deleteComercios(comercios: Comercios): Observable<Response> {
    return this.http.post<Response>(`${ipServer}/eliminarComercios`, comercios.id_comercio);
  }

  public updateComercio(comercios: Comercios): Observable<Response> {
    return this.http.put<Response>(`${ipServer}/actualizarComercios`, comercios);
  }
}
