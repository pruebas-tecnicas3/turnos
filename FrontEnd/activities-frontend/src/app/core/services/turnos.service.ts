import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Response} from "../entities/response";
import { Turnos } from '../entities/entidades';

export const ipServer = 'http://localhost:8085/api';

@Injectable({
  providedIn: 'root'
})
export class TurnosService {

  constructor(private http: HttpClient) {
  }

  public getAllTurnos(): Observable<Turnos[]> {
    return this.http.get<Turnos[]>(`${ipServer}/listTurnos`);
  }

  public registerTurnos(turnos: Turnos): Observable<Response> {
    debugger;
    return this.http.post<Response>(`${ipServer}/registrarTurno`, turnos);
  }

  public deleteTurnos(turnos: Turnos): Observable<Response> {
    return this.http.post<Response>(`${ipServer}/eliminarTurnos`, turnos.id_turno);
  }
}
