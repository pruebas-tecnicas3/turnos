import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Response} from "../entities/response";
import { Servicios } from '../entities/entidades';

export const ipServer = 'http://localhost:8085/api';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {

  constructor(private http: HttpClient) {
  }

  public getAllServicios(): Observable<Servicios[]> {
    return this.http.get<Servicios[]>(`${ipServer}/listServicios`);
  }

  public getServiciosPorIdComercio(idcomercio:number): Observable<Servicios[]> {
    return this.http.get<Servicios[]>(`${ipServer}/listServiciosPorComercio?idcomercio=${idcomercio}`);
  }

  public registerServicios(servicios: Servicios): Observable<Response> {
    debugger;
    return this.http.post<Response>(`${ipServer}/registrarServicios`, servicios);
  }

  public deleteServicios(servicios: Servicios): Observable<Response> {
    return this.http.post<Response>(`${ipServer}/eliminarServicios`, servicios.id_servicio);
  }

  public updateServicios(servicios: Servicios): Observable<Response> {
    return this.http.put<Response>(`${ipServer}/actualizarServicios`, servicios);
  }
}
