package com.ms.turnos.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ms.turnos.entities.Comercios;
import com.ms.turnos.services.ComerciosInterfaces;

@RestController
@RequestMapping(path = "${controller.properties.base-path}")
public class ComerciosController {

	@Autowired
    private ComerciosInterfaces comerciosService;

    @GetMapping("/listComercios")
    public List<Comercios> listAllComercios() {
        return comerciosService.findAll();
    }
    
    @PostMapping("/registrarComercios")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> registerComercios(@RequestBody Comercios comercios) {
        Map<String, Object> response = new HashMap<>();        
        try {        	
        	comerciosService.save(comercios);
            response.put("message", "Comercio creado con éxito.");
            response.put("objectResponse", comercios);
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (DataAccessException e) {
            response.put("message", "Error al hacer registro en la base de datos.");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PostMapping(value = "/eliminarComercios")
	public ResponseEntity<?> deleteComercios(@RequestBody long idComercios) {
    	Map<String, Object> response = new HashMap<>();
		try {
			comerciosService.delete(idComercios);
			response.put("message", "Comercio eliminado con éxito.");            
            return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (DataAccessException exception) {
			response.put("message", "Error al hacer la eliminacion en la base de datos.");
            response.put("error", exception.getMessage().concat(": ").concat(exception.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    
    @PutMapping(value = "/actualizarComercios")
	public ResponseEntity<?> updateComercios(@RequestBody Comercios comercios) {
    	Map<String, Object> response = new HashMap<>();
    	try {    		
    		comerciosService.update(comercios);			
			response.put("message", "Comercio actualizado con éxito.");
			response.put("objectResponse", comercios);
            return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (DataAccessException exception) {
			response.put("message", "Error al hacer la actualizacion en la base de datos.");
            response.put("error", exception.getMessage().concat(": ").concat(exception.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
