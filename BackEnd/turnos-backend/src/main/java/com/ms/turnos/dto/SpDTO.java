package com.ms.turnos.dto;

import com.ms.turnos.entities.Servicios;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor()
@NoArgsConstructor()
public class SpDTO {
	
	private String hora_inicio;
	private String hora_fin;
	private Servicios servicio;

}
