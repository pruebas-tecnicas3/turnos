package com.ms.turnos.services;

import java.util.List;
import com.ms.turnos.entities.Turnos;

public interface TurnosInterfaces {

	void accesoSP(String fecha_inicio, String fecha_fin, long id_servicio);
	
	List<Turnos> findAll();
	
	void delete(Long idTurnos);
}
