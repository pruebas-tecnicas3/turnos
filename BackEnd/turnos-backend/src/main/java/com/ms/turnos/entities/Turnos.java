package com.ms.turnos.entities;

import java.time.LocalDate;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@EqualsAndHashCode
@AllArgsConstructor
@Table(name = "turnos")
public class Turnos {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_turnos")
    @SequenceGenerator(name = "seq_turnos", sequenceName = "seq_turnos", initialValue = 1, allocationSize = 1)	
    @Column(name = "id_turno")
    private Long id_turno;
	@Column(name = "fecha_turno")
	private Date fecha_turno;
	@Column(name = "hora_inicio")
	private String hora_inicio;
	@Column(name = "hora_fin")
	private String hora_fin;
	@Column(name = "estado")
	private String estado;
	@JoinColumn(name = "id_servicios", referencedColumnName = "id_servicio", nullable = false)
    @ManyToOne(optional = false)
    private Servicios servicios;
	
	public Turnos() {
		
	}
}
