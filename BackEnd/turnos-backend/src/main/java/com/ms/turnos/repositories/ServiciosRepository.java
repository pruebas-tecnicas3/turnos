package com.ms.turnos.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ms.turnos.entities.Comercios;
import com.ms.turnos.entities.Servicios;
import java.util.List;


@Repository
public interface ServiciosRepository extends JpaRepository<Servicios, Long> {
	
	List<Servicios> findByComercio(Comercios comercio);
}
