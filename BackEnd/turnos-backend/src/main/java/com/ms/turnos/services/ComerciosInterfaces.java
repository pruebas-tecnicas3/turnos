package com.ms.turnos.services;

import java.util.List;

import com.ms.turnos.entities.Comercios;

public interface ComerciosInterfaces {
	
	List<Comercios> findAll();

    void save(Comercios comercios);
    
    void delete(Long idcomercios);
    
    Comercios update(Comercios comercios);    

    Comercios findById(Long id);

}
