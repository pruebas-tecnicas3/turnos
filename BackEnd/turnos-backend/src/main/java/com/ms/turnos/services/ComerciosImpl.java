package com.ms.turnos.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ms.turnos.entities.Comercios;
import com.ms.turnos.repositories.ComerciosRepository;

@Service
public class ComerciosImpl implements ComerciosInterfaces{

	@Autowired
    private ComerciosRepository comerciosRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Comercios> findAll() {
        return comerciosRepository.findAll();
    }
    
    @Override
    @Transactional(readOnly = true)
    public Comercios findById(Long id) {
        return comerciosRepository.findById(id).orElse(null);
    }

    @Override
    @Transactional
    public void save(Comercios comercios) {    	
    	comerciosRepository.save(comercios);
    }
    
    @Override
    @Transactional
    public void delete(Long idComercios) {
    	comerciosRepository.deleteById(idComercios);
    }
    
    @Override
    @Transactional
    public Comercios update(Comercios comercios) {    	
    	Comercios comerciosUpdate = comerciosRepository.findById(comercios.getId_comercio()).orElse(null);
    	comerciosUpdate.setNom_comercio(comercios.getNom_comercio());
    	comerciosUpdate.setAforo_maximo(comercios.getAforo_maximo());    	
    	comerciosRepository.save(comerciosUpdate);
        return comerciosUpdate;
    }
}
