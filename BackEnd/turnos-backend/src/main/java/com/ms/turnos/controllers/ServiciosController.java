package com.ms.turnos.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ms.turnos.entities.Comercios;
import com.ms.turnos.entities.Servicios;
import com.ms.turnos.services.ServiciosInterfaces;

@RestController
@RequestMapping(path = "${controller.properties.base-path}")
public class ServiciosController {
	
	@Autowired
    private ServiciosInterfaces serviciosService;

    @GetMapping("/listServicios")
    public List<Servicios> listAllservicios() {
        return serviciosService.findAll();
    }
    
    @GetMapping("/listServiciosPorComercio")
    public List<Servicios> listServiciosPorComercio(@RequestParam long idcomercio) {
    	Comercios c = new Comercios();
    	c.setId_comercio(idcomercio);
        return serviciosService.findByComercios(c);
    }
    
    @PostMapping("/registrarServicios")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> registerServicios(@RequestBody Servicios servicios) {
        Map<String, Object> response = new HashMap<>();        
        try {        	
        	serviciosService.save(servicios);
            response.put("message", "Servicio creado con éxito.");
            response.put("objectResponse", servicios);
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (DataAccessException e) {
            response.put("message", "Error al hacer registro en la base de datos.");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PostMapping(value = "/eliminarServicios")
	public ResponseEntity<?> deleteServicios(@RequestBody long idServicios) {
    	Map<String, Object> response = new HashMap<>();
		try {
			serviciosService.delete(idServicios);
			response.put("message", "Servicio eliminado con éxito.");            
            return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (DataAccessException exception) {
			response.put("message", "Error al hacer la eliminacion en la base de datos.");
            response.put("error", exception.getMessage().concat(": ").concat(exception.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    
    @PutMapping(value = "/actualizarServicios")
	public ResponseEntity<?> updateServicios(@RequestBody Servicios servicios) {
    	Map<String, Object> response = new HashMap<>();
    	try {    		
    		serviciosService.update(servicios);			
			response.put("message", "Servicio actualizado con éxito.");
			response.put("objectResponse", servicios);
            return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (DataAccessException exception) {
			response.put("message", "Error al hacer la actualizacion en la base de datos.");
            response.put("error", exception.getMessage().concat(": ").concat(exception.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
