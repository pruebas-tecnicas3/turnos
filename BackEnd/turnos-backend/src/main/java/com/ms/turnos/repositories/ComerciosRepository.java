package com.ms.turnos.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ms.turnos.entities.Comercios;

@Repository
public interface ComerciosRepository extends JpaRepository<Comercios, Long> {

}
