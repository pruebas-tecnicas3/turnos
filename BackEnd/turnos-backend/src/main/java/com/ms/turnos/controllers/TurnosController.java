package com.ms.turnos.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.ms.turnos.dto.SpDTO;
import com.ms.turnos.entities.Turnos;
import com.ms.turnos.services.TurnosInterfaces;

@RestController
@RequestMapping(path = "${controller.properties.base-path}")
public class TurnosController {
	
	@Autowired
    private TurnosInterfaces turnosService;

	@GetMapping("/listTurnos")
    public List<Turnos> listAllTurnos() {
        return turnosService.findAll();
    }
	
	@PostMapping("/registrarTurno")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> registerComercios(@RequestBody SpDTO spDTO) {
        Map<String, Object> response = new HashMap<>();        
        try {        	
        	turnosService.accesoSP(spDTO.getHora_inicio(), spDTO.getHora_fin(), spDTO.getServicio().getId_servicio());
            response.put("message", "SP ejecutado con éxito.");
            response.put("objectResponse", "Se registró turno");
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (DataAccessException e) {
            response.put("message", "Error al hacer registro en la base de datos.");
            response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
	
	@PostMapping(value = "/eliminarTurnos")
	public ResponseEntity<?> deleteComercios(@RequestBody long idServicios) {
    	Map<String, Object> response = new HashMap<>();
		try {
			turnosService.delete(idServicios);
			response.put("message", "Comercio eliminado con éxito.");            
            return new ResponseEntity<>(response, HttpStatus.OK);
		} catch (DataAccessException exception) {
			response.put("message", "Error al hacer la eliminacion en la base de datos.");
            response.put("error", exception.getMessage().concat(": ").concat(exception.getMostSpecificCause().getMessage()));
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
