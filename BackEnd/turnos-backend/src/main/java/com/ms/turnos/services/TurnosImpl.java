package com.ms.turnos.services;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

import java.util.List;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.ms.turnos.entities.Turnos;
import com.ms.turnos.repositories.TurnosRepository;

@Service
public class TurnosImpl implements TurnosInterfaces{

	@Autowired
    EntityManager entityManager;
	private Logger log = Logger.getLogger("Log Turnos");
	
	@Autowired
    private TurnosRepository turnosRepository;

	@Override
    @Transactional(readOnly = true)
    public List<Turnos> findAll() {
        return turnosRepository.findAll();
    }
	
	@Override
    @Transactional
    public void delete(Long idTurnos) {
		turnosRepository.deleteById(idTurnos);
    }
	
    @Override
    public void accesoSP(String fecha_inicio, String fecha_fin, long id_servicio){
    	try{
            StoredProcedureQuery proc = entityManager.createStoredProcedureQuery("ASESOFTWARE.CREATETURNO");
            proc.registerStoredProcedureParameter("FECHA_INICIO", String.class, ParameterMode.IN);
            proc.registerStoredProcedureParameter("FECHA_FIN", String.class, ParameterMode.IN);
            proc.registerStoredProcedureParameter("ID_SERVICIO", Long.class, ParameterMode.IN);
            proc.setParameter("FECHA_INICIO", fecha_inicio);
            proc.setParameter("FECHA_FIN", fecha_fin);
            proc.setParameter("ID_SERVICIO", id_servicio);
            proc.execute();
    	}catch (Exception e){
    		log.info("ERROR SP:"+e.getMessage().toString());
        }
    }
}
