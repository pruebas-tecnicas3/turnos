package com.ms.turnos.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@AllArgsConstructor
@Table(name = "comercios")
public class Comercios {
	
	@Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_comercios")
    @SequenceGenerator(name = "seq_comercios", sequenceName = "seq_comercios", initialValue = 1, allocationSize = 1)	
    @Column(name = "id_comercio")
    private Long id_comercio;
	@Column(name = "nom_comercio")
    private String nom_comercio;
	@Column(name = "aforo_maximo")
    private int aforo_maximo;

	public Comercios(){
		
	}
}
