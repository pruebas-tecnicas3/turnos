package com.ms.turnos.services;

import java.util.List;

import com.ms.turnos.entities.Comercios;
import com.ms.turnos.entities.Servicios;

public interface ServiciosInterfaces {
	
	List<Servicios> findAll();

    void save(Servicios servicios);
    
    void delete(Long idServicios);
    
    Servicios update(Servicios servicios);    

    Servicios findById(Long id);
    
    List<Servicios> findByComercios(Comercios comercio);

}
